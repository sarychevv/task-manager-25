package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.model.Task;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by project id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        if (tasks == null) throw new TaskNotFoundException();
        renderTasks(tasks);
    }

}
