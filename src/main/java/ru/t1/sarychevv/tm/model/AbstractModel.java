package ru.t1.sarychevv.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractModel {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    protected Date created = new Date();

}
