package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public void create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        add(task);
    }

    public void create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll(userId)
                .stream()
                .filter(r -> r.getProjectId() != null)
                .filter(r -> projectId.equals(r.getProjectId()))
                .collect(Collectors.toList());
    }

}
